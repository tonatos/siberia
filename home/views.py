from django.shortcuts import render
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.http import JsonResponse, Http404
from django.shortcuts import get_object_or_404
from django.views.decorators.csrf import csrf_exempt

from .models import ApplicantItem, SendPhoto

@csrf_exempt
def vote(request):
    if request.POST:
        pk = request.POST['pk']

        if 'vote-' + pk in request.session:
            return JsonResponse({'status': False, 'error': 'an attempt to re-vote'})

        applicant = get_object_or_404(ApplicantItem, pk=pk)
        applicant.votes += 1
        applicant.save()

        request.session['vote-' + pk] = True
        return JsonResponse({'status': True, 'votes': applicant.votes})

    return JsonResponse({'status': False})


@csrf_exempt
def photo_vote(request):
    if request.POST:
        pk = request.POST['pk']

        if 'photo-vote-' + pk in request.session or 'photo-vote' in request.session:
            return JsonResponse({'status': False, 'error': 'an attempt to re-vote'})

        applicant = get_object_or_404(SendPhoto, pk=pk)
        applicant.votes += 1
        applicant.save()

        request.session['photo-vote'] = True
        request.session['photo-vote-' + pk] = True
        return JsonResponse({'status': True, 'votes': applicant.votes})

    return JsonResponse({'status': False})