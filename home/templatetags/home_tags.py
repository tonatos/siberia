# -*- coding: utf-8 -*-

from itertools import izip_longest
import random

from django import template
from django.shortcuts import render
from django.core.mail import EmailMessage
from django.utils.six import text_type
from django.db.models.query_utils import Q

from home.models import NewsItemPage, DestinyItemPage, MaterialsItemPage, Region, SiteSettings, PersonPage, SendPhoto
from home.forms import HistoryForm, PhotoForm

from wagtail.wagtailimages.models import get_image_model

register = template.Library()

@register.assignment_tag(takes_context=True)
def get_site_root(context):
    # NB this returns a core.Page, not the implementation-specific model used
    # so object-comparison to self will return false as objects would differ
    return context['request'].site.root_page

def has_menu_children(page):
    return page.get_children().live().in_menu().exists()

@register.inclusion_tag('tags/pages_menu.html', takes_context=True)
def pages_menu(context, parent, calling_page=None):
    menuitems = parent.get_children().filter(
        live=True,
        show_in_menus=True
    )
    if calling_page:
        active_parent = calling_page.get_parent()
        for i, menuitem in enumerate(menuitems):
            menuitems[i].show_dropdown = has_menu_children(menuitem)
            if menuitem.title == calling_page.title \
                or menuitem.title == active_parent.title:
                menuitems[i].active = True

    return {
        'calling_page': calling_page,
        'menuitems': menuitems,
        'request': context['request'],
    }

@register.inclusion_tag('tags/top_menu_children.html', takes_context=True)
def top_menu_children(context, parent):
    menuitems_children = parent.get_children()
    menuitems_children = menuitems_children.live().in_menu()
    return {
        'parent': parent,
        'menuitems_children': menuitems_children,
        'request': context['request'],
    }

@register.inclusion_tag('tags/bottom_menu.html', takes_context=True)
def bottom_menu(context, parent, calling_page=None):
    menuitems = parent.get_children().filter(
        live=True,
        show_in_menus=True
    )
    if calling_page:
        active_parent = calling_page.get_parent()
        for i, menuitem in enumerate(menuitems):
            if menuitem.title == calling_page.title \
                or menuitem.title == active_parent.title:
                menuitems[i].active = True

    return {
        'calling_page': calling_page,
        'menuitems': menuitems,
        'request': context['request'],
    }


@register.filter('grouper')
def grouper(value, arg):
    izip_args = [iter(value)] * arg
    ret = list(izip_longest(fillvalue=None, *izip_args))
    return ret


@register.inclusion_tag('tags/found_news.html', takes_context=True)
def get_found_news(context, is_white=False):
    return {
        'request': context['request'],
        'is_white': is_white,
        'items': NewsItemPage.objects.live()\
                             .filter(is_foundation=True)\
                             .order_by('-first_published_at')[:12]
    }


@register.inclusion_tag('tags/index_destiny.html', takes_context=True)
def get_index_destiny(context):
    return {
        'request': context['request'],
        'items': DestinyItemPage.objects.live()\
                                .order_by('-first_published_at')[:3]
    }


@register.inclusion_tag('tags/index_materials.html', takes_context=True)
def get_index_materials(context):
    try:
        person = PersonPage.objects.live().get(slug='founder')
    except (PersonPage.DoesNotExist):
        person = None

    return {
        'request': context['request'],
        'person': person,
        'items': MaterialsItemPage.objects.live()\
                                  .order_by('-first_published_at')[:9]
    }


@register.inclusion_tag('tags/index_news.html', takes_context=True)
def get_index_news(context):
    rubric_items = NewsItemPage.objects\
                        .order_by('-first_published_at')\
                        .raw('SELECT * FROM home_newsitempage WHERE rubric_id != null GROUP BY rubric_id')

    queryset = NewsItemPage.objects.live().order_by('-first_published_at').exclude(id__in=[i.id for i in rubric_items])
    region = context['request'].GET.get('region', None)

    if region and region != '0':
        queryset = queryset.filter(Q(region__pk=region)|Q(region=None))
    try:
        main_news = queryset.filter(important=True)[0]
    except (IndexError):
        main_news = None

    items = queryset.exclude(main_image=None).exclude(pk=main_news.pk if main_news else 0)[:4]
    ribbon = queryset.exclude(pk__in=[i.pk for i in items] + [main_news.pk if main_news else 0])[:18]

    return {
        'request': context['request'],
        'main_news': main_news,
        'items':items,
        'rubric_items': rubric_items,
        'ribbon': ribbon,
        'regions': Region.objects.order_by('title')
    }


@register.inclusion_tag('tags/recommend_materials.html', takes_context=True)
def get_recommend_materials(context, exclude=None):
    return {
        'request': context['request'],
        'items': MaterialsItemPage.objects.live()\
                                  .order_by('-first_published_at')\
                                  .exclude(pk=exclude.pk)[:9]
    }


@register.inclusion_tag('tags/recommend_news.html', takes_context=True)
def get_recommend_news(context, exclude=None):
    return {
        'request': context['request'],
        'items': NewsItemPage.objects.live()\
                             .order_by('-first_published_at')\
                             .exclude(main_image=None)\
                             .exclude(pk=exclude.pk)[:12]
    }


@register.inclusion_tag('tags/destiny_form.html', takes_context=True)
def destiny_form(context):
    if context['request'].POST:
        form = HistoryForm(context['request'].POST)
        
        if form.is_valid():
            settings = SiteSettings.for_site(context['request'].site)

            from_address = 'noreply@sibhar.ru'
            content = '\n'.join([x[1].label + ' ' + text_type(form.data.get(x[0])) for x in form.fields.items()])
            email = EmailMessage(
                u'История с сайта «Сибирский характер»', 
                content, from_address, settings.email_to.split(', '))

            files = context['request'].FILES
            for i in files:
                email.attach(files[i].name, files[i].read(), files[i].content_type)

            email.send()

            return {
                'request': context['request']
            }
    else:
        form = HistoryForm()

    return {
        'request': context['request'],
        'form': form,
    }

@register.inclusion_tag('tags/photo_form.html', takes_context=True)
def photo_form(context):
    if context['request'].POST:
        form = PhotoForm(context['request'].POST)
        
        if form.is_valid():
            settings = SiteSettings.for_site(context['request'].site)

            from_address = 'noreply@sibhar.ru'
            content = '\n'.join([x[1].label + ' ' + text_type(form.data.get(x[0])) for x in form.fields.items()])
            email = EmailMessage(
                u'Фотография на конкурс «Сибирский характер»', 
                content, from_address, settings.email_to.split(', '))

            files = context['request'].FILES
            for i in files:
                print i
                email.attach(files[i].name, files[i].read(), files[i].content_type)
            
            photo = SendPhoto(**{
                'email': context['request'].POST['email'],
                'name': context['request'].POST['name'],
                'content': context['request'].POST['history'],
                'main_image': context['request'].FILES['photo']
            })
            photo.save()

            email.send()

            return {
                'request': context['request']
            }
    else:
        form = PhotoForm()

    return {
        'request': context['request'],
        'form': form,
    }

@register.inclusion_tag('tags/get_photos.html', takes_context=True)
def get_photos(context):
    return {
        'items': SendPhoto.objects.exclude(main_image=False).filter(public=True).order_by('created'),
        'voted': 1 if 'photo-vote' in context['request'].session else 0
    }

@register.filter('in_session')
def in_session(request, pk):
    return 'vote-' + str(pk) in request.session
