# -*- coding: utf-8 -*-

from __future__ import unicode_literals
from datetime import date
import datetime

from django.db import models
from django.shortcuts import render
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.db.models.query_utils import Q
from django import forms

from modelcluster.fields import ParentalKey

from wagtail.wagtailcore.models import Page
from wagtail.wagtailcore.fields import RichTextField
from wagtail.wagtailadmin.edit_handlers import FieldPanel
from wagtail.wagtailimages.edit_handlers import ImageChooserPanel
from wagtail.wagtailforms.models import AbstractEmailForm, AbstractFormField
from wagtail.contrib.settings.models import BaseSetting, register_setting
from wagtail.wagtailadmin.edit_handlers import InlinePanel, MultiFieldPanel


class DashboardMixin(object):
    def serve(self, request):
        region = request.GET.get('region', 0)
        rubric = request.GET.get('rubric', 0)
        page = request.GET.get('page', 1)

        regions = Region.objects.order_by('title')
        rubrics = NewsRubric.objects.order_by('title')
        items = self.model_items.objects.live().order_by('-first_published_at')
        
        if hasattr(self.model_items, 'region') and int(region) is not 0:
            items = items.filter(Q(region=region)|Q(region=None))

        if hasattr(self.model_items, 'rubric') and int(rubric) is not 0:
            items = items.filter(Q(rubric=rubric))

        # Pagination
        paginator = Paginator(items, 18)
        try:
            items = paginator.page(page)
        except PageNotAnInteger:
            items = paginator.page(1)
        except EmptyPage:
            items = paginator.page(paginator.num_pages)

        return render(request, self.template, {
            'self': self,
            'items': items,
            'regions': regions,
            'rubrics': rubrics,
            'active_page': page
        })


class Feedback(models.Model):
    email = models.CharField(u'Эл. почта', max_length=255)
    name = models.CharField(u'ФИО авторы', max_length=255)
    content = models.CharField(u'Отзыв', max_length=255)
    public = models.BooleanField(u'Опубликован', default=False)
    created = models.DateTimeField(u'Дата добавления',
                                   auto_now=False, auto_now_add=True, null=True,
                                   blank = False)
    class Meta:
        verbose_name = u'Отзыв'
        verbose_name_plural = u'Отзывы'


class SendPhoto(models.Model):
    email = models.CharField(u'Эл. почта', max_length=255)
    name = models.CharField(u'ФИО автора', max_length=255)
    content = models.CharField(u'Описание фотографии', max_length=255)
    main_image = models.ImageField(
        verbose_name=u'Фотография',
        null=True,
        blank=True,
        upload_to='uploads/%Y/%m/%d/')
    votes = models.IntegerField(u'Голоса', default=0, blank=True)
    public = models.BooleanField(u'Опубликован', default=False)
    created = models.DateTimeField(u'Дата добавления',
                                   auto_now=False, auto_now_add=True, null=True,
                                   blank = False)
    class Meta:
        verbose_name = u'Отзыв'
        verbose_name_plural = u'Отзывы'



class Rubric(models.Model):
    title = models.CharField(u'Название', max_length=255)

    def __unicode__(self):
        return self.title

    class Meta:
        verbose_name = u'Рубрика'
        verbose_name_plural = u'Рубрики'


class NewsRubric(models.Model):
    title = models.CharField(u'Название', max_length=255)

    def __unicode__(self):
        return self.title

    class Meta:
        verbose_name = u'Рубрика'
        verbose_name_plural = u'Рубрики'

class Region(models.Model):
    title = models.CharField(u'Название', max_length=255)

    def __unicode__(self):
        return self.title

    class Meta:
        verbose_name = u'Регион'
        verbose_name_plural = u'Регионы'

class HomePage(Page):
    template = './pages/index.html'
    class Meta:
        verbose_name = u'Главная'
        verbose_name_plural = u'Главные'

class PersonPage(Page):
    quote = models.TextField(u'Цитата', blank=True)
    content = RichTextField(u'Биография', blank=True)
    
    subpage_types = None
    template = './pages/person.html'
    content_panels = Page.content_panels + [
        FieldPanel('quote'),
        FieldPanel('content'),
    ]
    class Meta:
        verbose_name = u'Страница Андрея Владимировича'
        verbose_name_plural = u'Страницы Андрея Владимировича'



class DestinyItemPage(Page):
    description = models.CharField(u'Кто он?', max_length=255, blank=True)
    content = RichTextField(u'Биография (история)', blank=True)
    main_image = models.ForeignKey(
        'wagtailimages.Image',
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        verbose_name=u'Картинки',
        related_name='%(app_label)s_%(class)s_main_image'
    )

    template = './pages/destiny-item.html'
    subpage_types = None
    content_panels = AbstractEmailForm.content_panels + [
        FieldPanel('description'),
        FieldPanel('content'),
        ImageChooserPanel('main_image'),
    ]
    class Meta:
        verbose_name = u'Судьба'
        verbose_name_plural = u'Судьбы'


class DestinyPage(DashboardMixin, Page):
    model_items = DestinyItemPage
    subpage_types = ['DestinyItemPage',]
    template = './pages/destiny.html'
    class Meta:
        verbose_name = u'Дашборд судеб'
        verbose_name_plural = u'Дашборды судеб'




class NewsItemPage(Page):
    content = RichTextField(u'Контент', blank=True)
    region = models.ForeignKey(
        Region,
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        verbose_name=u'Регион',
        related_name='%(app_label)s_%(class)s_region')

    main_image = models.ForeignKey(
        'wagtailimages.Image',
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        verbose_name=u'Изображение новости',
        related_name='%(app_label)s_%(class)s_main_image')

    autor = models.CharField(u'Автор', max_length=100, blank=True)
    source = models.CharField(u'Источник', max_length=100, blank=True)
    is_foundation = models.BooleanField(u'Новость фонда', default=False)
    important = models.BooleanField(u'Важная новость', default=False, 
                        help_text=u'Будет показываться на главной тизером с картинкой')

    rubric = models.ForeignKey(
        NewsRubric,
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        verbose_name=u'Рубрика',
        related_name='%(app_label)s_%(class)s_rubric')

    template = './pages/news-item.html'
    subpage_types = None
    content_panels = Page.content_panels + [
        FieldPanel('content'),
        ImageChooserPanel('main_image'),
        FieldPanel('region'),
        FieldPanel('autor'),
        FieldPanel('source'),
        FieldPanel('rubric'),
        FieldPanel('is_foundation'),
        FieldPanel('important'),
    ]
    class Meta:
        verbose_name = u'Новость'
        verbose_name_plural = u'Новости'

    def is_today(self):
        if date.today() > self.first_published_at.date():
            return False
        return True


class NewsPage(DashboardMixin, Page):
    model_items = NewsItemPage
    template = './pages/news.html'
    subpage_types = ['NewsItemPage',]

    class Meta:
        verbose_name = u'Дашборд новостей'
        verbose_name_plural = u'Дашборды новостей'

    def serve(self, request):
        request = super(NewsPage, self).serve(request)
        return request


class MaterialsItemPage(Page):
    content = RichTextField(u'Контент', blank=True)
    rubric = models.ForeignKey(
        Rubric,
        null=True,
        blank=False,
        on_delete=models.SET_NULL,
        verbose_name=u'Рубрика',
        related_name='%(app_label)s_%(class)s_rubric')

    main_image = models.ForeignKey(
        'wagtailimages.Image',
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        verbose_name=u'Изображение материала',
        related_name='%(app_label)s_%(class)s_main_image'
    )
    autor = models.CharField(u'Автор', max_length=100, blank=True)
    source = models.CharField(u'Источник', max_length=100, blank=True)

    template = './pages/material-item.html'
    subpage_types = None
    content_panels = Page.content_panels + [
        FieldPanel('content'),
        ImageChooserPanel('main_image'),
        FieldPanel('rubric'),
        FieldPanel('autor'),
        FieldPanel('source'),
    ]
    class Meta:
        verbose_name = u'Материал'
        verbose_name_plural = u'Материалы'


class MaterialsPage(DashboardMixin, Page):
    model_items = MaterialsItemPage
    template = './pages/material.html'
    subpage_types = ['MaterialsItemPage',]

    class Meta:
        verbose_name = u'Дашборд материалов'
        verbose_name_plural = u'Дашбоорды материалов'



class FundPage(Page):
    content = RichTextField(u'Контент', blank=True)

    template = './pages/fund.html'
    subpage_types = ['StaticPage', 'PersonPage', 'ApplicantList']
    content_panels = Page.content_panels + [
        FieldPanel('content'),
    ]
    class Meta:
        verbose_name = u'Страницы фонда'
        verbose_name_plural = u'Страницы фонда'

class FormField(AbstractFormField):
    page = ParentalKey('ContactPage', related_name='form_fields')

class ContactPage(AbstractEmailForm):
    intro = RichTextField(u'Вступительный текст', blank=True)
    thank_you_text = RichTextField(u'Текст на странице после отправки формы', blank=True)

    template = './pages/contact.html'
    subpage_types = None

    def serve(self, request):
        if request.method == 'POST':
            form = self.get_form(request.POST)

            if form.is_valid():
                self.process_form_submission(form)

                feedback = Feedback(**{
                        'email': request.POST['pochta-dlia-obratnoi-sviazi'],
                        'name': request.POST['vashe-imia'],
                        'content': request.POST['vash-otzyv']
                    })
                feedback.save()

                return render(
                    request,
                    self.landing_page_template,
                    self.get_context(request)
                )
        else:
            form = self.get_form()

        return render(request, self.template, {
            'self': self,
            'form': form,
            'items': Feedback.objects.filter(public=True).order_by('-created')
        })

    class Meta:
        verbose_name = u'Контакты'
        verbose_name_plural = u'Контакты'

ContactPage.content_panels = [
    FieldPanel('title', classname="full title"),
    FieldPanel('intro', classname="full"),
    InlinePanel('form_fields', label=u"Поля формы"),
    FieldPanel('thank_you_text', classname="full"),
    MultiFieldPanel([
        FieldPanel('to_address', classname="full"),
        FieldPanel('from_address', classname="full"),
        FieldPanel('subject', classname="full"),
    ], "Email")
]

class StaticPage(Page):
    content = RichTextField(u'Контент', blank=True)

    template = './pages/static.html'
    subpage_types = ['StaticPage',]
    content_panels = Page.content_panels + [
        FieldPanel('content'),
    ]
    class Meta:
        verbose_name = u'Статическая страница'
        verbose_name_plural = u'Статические страницы'


class ApplicantItem(Page):
    photo = models.ForeignKey(
        'wagtailimages.Image',
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        verbose_name=u'Фотография',
        related_name='%(app_label)s_%(class)s_photo'
    )
    source = models.CharField(u'Район', max_length=150, blank=True)
    content = RichTextField(u'Текстовое содержимое', blank=True)

    votes = models.IntegerField(u'Голоса', default=0, blank=True)

    template = './pages/applicant-item.html'
    subpage_types = None
    content_panels = Page.content_panels + [
        FieldPanel('content'),
        FieldPanel('source'),
        ImageChooserPanel('photo'),
        FieldPanel('votes', widget=forms.TextInput(attrs={
            'disabled': 'true',
            'style': "color: #000",
            'required': False})),
    ]

    class Meta:
        verbose_name = u'Кандидат'
        verbose_name_plural = u'Кандидаты'


class ApplicantList(DashboardMixin, Page):
    content = RichTextField(u'Текстовое содержимое', blank=True)

    model_items = ApplicantItem
    template = './pages/applicants.html'
    subpage_types = ['ApplicantItem',]
    content_panels = Page.content_panels + [
        FieldPanel('content'),
    ]

    class Meta:
        verbose_name = u'Список кандидатов'
        verbose_name_plural = u'Списки кандидатов'
    
class Competition(Page):
    content = models.TextField(u'Текстовое содержимое', blank=True)

    template = './pages/competition.html'
    subpage_types = ['CompetitionForm', 'StaticPage']
    content_panels = Page.content_panels + [
        FieldPanel('content'),
    ]

    class Meta:
        verbose_name = u'Фотоконкурс'
        verbose_name_plural = u'Фотоконкурсы'
    

class CompetitionForm(Page):
    intro = RichTextField(u'Требования к фотографиям', blank=True)

    template = './pages/competition-form.html'
    subpage_types = None

    class Meta:
        verbose_name = u'Форма отправки фотографии'
        verbose_name_plural = u'Формы конкурса'
    

@register_setting
class SiteSettings(BaseSetting):
    found_fb = models.URLField(u'Фэйсбук фонда', 
        help_text=u'Адрес страницы в фэйсбуке', blank=True)
    found_vk = models.URLField(u'ВКонтакте фонда', 
        help_text=u'Адрес страницы ВКонтакте', blank=True)
    found_ok = models.URLField(u'Одноклассники фонда', 
        help_text=u'Адрес страницы в одноклассниках', blank=True)

    candidate_fb = models.URLField(u'Фэйсбук кандидата', 
        help_text=u'Адрес страницы в фэйсбуке', blank=True)
    candidate_vk = models.URLField(u'ВКонтакте кандидата', 
        help_text=u'Адрес страницы ВКонтакте', blank=True)
    candidate_ok = models.URLField(u'Одноклассники кандидата', 
        help_text=u'Адрес страницы в одноклассниках', blank=True)

    email_to = models.CharField(u'Эл. почта для писем', max_length=255, 
            help_text=u'Можно перечислять несколько через запятую')
    
    class Meta:
        verbose_name = 'Настройки сайта'