# -*- coding: utf-8 -*-
# Generated by Django 1.9.3 on 2016-03-08 13:40
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('home', '0007_auto_20160307_1818'),
    ]

    operations = [
        migrations.AddField(
            model_name='materialsitempage',
            name='autor',
            field=models.CharField(blank=True, max_length=100, verbose_name='\u0410\u0432\u0442\u043e\u0440'),
        ),
        migrations.AddField(
            model_name='materialsitempage',
            name='source',
            field=models.CharField(blank=True, max_length=100, verbose_name='\u0418\u0441\u0442\u043e\u0447\u043d\u0438\u043a'),
        ),
        migrations.AddField(
            model_name='newsitempage',
            name='autor',
            field=models.CharField(blank=True, max_length=100, verbose_name='\u0410\u0432\u0442\u043e\u0440'),
        ),
        migrations.AddField(
            model_name='newsitempage',
            name='source',
            field=models.CharField(blank=True, max_length=100, verbose_name='\u0418\u0441\u0442\u043e\u0447\u043d\u0438\u043a'),
        ),
    ]
