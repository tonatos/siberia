# -*- coding: utf-8 -*-
# Generated by Django 1.9.3 on 2016-03-13 07:33
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('home', '0009_auto_20160308_1518'),
    ]

    operations = [
        migrations.AlterField(
            model_name='sitesettings',
            name='candidate_fb',
            field=models.URLField(blank=True, help_text='\u0410\u0434\u0440\u0435\u0441 \u0441\u0442\u0440\u0430\u043d\u0438\u0446\u044b \u0432 \u0444\u044d\u0439\u0441\u0431\u0443\u043a\u0435', verbose_name='\u0424\u044d\u0439\u0441\u0431\u0443\u043a \u043a\u0430\u043d\u0434\u0438\u0434\u0430\u0442\u0430'),
        ),
        migrations.AlterField(
            model_name='sitesettings',
            name='candidate_ok',
            field=models.URLField(blank=True, help_text='\u0410\u0434\u0440\u0435\u0441 \u0441\u0442\u0440\u0430\u043d\u0438\u0446\u044b \u0432 \u043e\u0434\u043d\u043e\u043a\u043b\u0430\u0441\u0441\u043d\u0438\u043a\u0430\u0445', verbose_name='\u041e\u0434\u043d\u043e\u043a\u043b\u0430\u0441\u0441\u043d\u0438\u043a\u0438 \u043a\u0430\u043d\u0434\u0438\u0434\u0430\u0442\u0430'),
        ),
        migrations.AlterField(
            model_name='sitesettings',
            name='candidate_vk',
            field=models.URLField(blank=True, help_text='\u0410\u0434\u0440\u0435\u0441 \u0441\u0442\u0440\u0430\u043d\u0438\u0446\u044b \u0412\u041a\u043e\u043d\u0442\u0430\u043a\u0442\u0435', verbose_name='\u0412\u041a\u043e\u043d\u0442\u0430\u043a\u0442\u0435 \u043a\u0430\u043d\u0434\u0438\u0434\u0430\u0442\u0430'),
        ),
        migrations.AlterField(
            model_name='sitesettings',
            name='found_fb',
            field=models.URLField(blank=True, help_text='\u0410\u0434\u0440\u0435\u0441 \u0441\u0442\u0440\u0430\u043d\u0438\u0446\u044b \u0432 \u0444\u044d\u0439\u0441\u0431\u0443\u043a\u0435', verbose_name='\u0424\u044d\u0439\u0441\u0431\u0443\u043a \u0444\u043e\u043d\u0434\u0430'),
        ),
        migrations.AlterField(
            model_name='sitesettings',
            name='found_ok',
            field=models.URLField(blank=True, help_text='\u0410\u0434\u0440\u0435\u0441 \u0441\u0442\u0440\u0430\u043d\u0438\u0446\u044b \u0432 \u043e\u0434\u043d\u043e\u043a\u043b\u0430\u0441\u0441\u043d\u0438\u043a\u0430\u0445', verbose_name='\u041e\u0434\u043d\u043e\u043a\u043b\u0430\u0441\u0441\u043d\u0438\u043a\u0438 \u0444\u043e\u043d\u0434\u0430'),
        ),
        migrations.AlterField(
            model_name='sitesettings',
            name='found_vk',
            field=models.URLField(blank=True, help_text='\u0410\u0434\u0440\u0435\u0441 \u0441\u0442\u0440\u0430\u043d\u0438\u0446\u044b \u0412\u041a\u043e\u043d\u0442\u0430\u043a\u0442\u0435', verbose_name='\u0412\u041a\u043e\u043d\u0442\u0430\u043a\u0442\u0435 \u0444\u043e\u043d\u0434\u0430'),
        ),
    ]
