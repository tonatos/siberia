# -*- coding: utf-8 -*-
from django.conf import settings
from django.utils.html import format_html, format_html_join

from wagtail.wagtailcore import hooks
from wagtailmodeladmin.options import ModelAdmin, wagtailmodeladmin_register
from wagtail.wagtailcore.whitelist import attribute_rule, check_url, allow_without_attributes
from .models import Rubric, NewsRubric, Region, Feedback, SendPhoto


class RubricAdmin(ModelAdmin):
    model = Rubric
    menu_label = u'Рубрики' # ditch this to use verbose_name_plural from model
    menu_icon = '' # change as required
    menu_order = 200 # will put in 3rd place (000 being 1st, 100 2nd)
    add_to_settings_menu = False # or True to add your model to the Settings sub-menu
    list_display = ('title',)
wagtailmodeladmin_register(RubricAdmin)

class NewsRubricAdmin(ModelAdmin):
    model = NewsRubric
    menu_label = u'Рубрики новостей' # ditch this to use verbose_name_plural from model
    menu_icon = '' # change as required
    menu_order = 250 # will put in 3rd place (000 being 1st, 100 2nd)
    add_to_settings_menu = False # or True to add your model to the Settings sub-menu
    list_display = ('title',)
wagtailmodeladmin_register(NewsRubricAdmin)

class RegionAdmin(ModelAdmin):
    model = Region
    menu_label = u'Регионы'
    menu_icon = ''
    menu_order = 200
    add_to_settings_menu = False
    list_display = ('title',)
wagtailmodeladmin_register(RegionAdmin)


class FeedbackAdmin(ModelAdmin):
    model = Feedback
    menu_label = u'Отзывы'
    menu_icon = ''
    menu_order = 300
    add_to_settings_menu = False
    list_display = ('name', 'content', 'public')
    list_editable = ('public',)
    list_filter = ('public',)
wagtailmodeladmin_register(FeedbackAdmin)


class SendPhotoAdmin(ModelAdmin):
    model = SendPhoto
    menu_label = u'Конкурс'
    menu_icon = ''
    menu_order = 300
    add_to_settings_menu = False
    list_display = ('name', 'content', 'public', 'votes')
    list_editable = ('public',)
    list_filter = ('public',)
wagtailmodeladmin_register(SendPhotoAdmin)



@hooks.register('construct_whitelister_element_rules')
def whitelister_element_rules():
    return {
        'a': attribute_rule({'href': check_url, 'target': True, 'name': True}),
    }

@hooks.register('insert_editor_js')
def editor_js():
  return format_html(
    """
    <script>
      registerHalloPlugin('hallohtml');
    </script>
    """
  )