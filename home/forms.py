# -*- coding: utf-8 -*-

from django import forms

class HistoryForm(forms.Form):
	name = forms.CharField(label=u'Ваше имя:', max_length=140)
	email = forms.EmailField(label=u'Почта для обратной связи:', max_length=140)
	who = forms.CharField(label=u'Про кого история?', max_length=140)
	photo = forms.ImageField(label=u'Приложите фотографию:', max_length=100, required=False)
	history = forms.CharField(label=u'Расскажите историю:', max_length=1000, widget=forms.Textarea)

class PhotoForm(forms.Form):
	name = forms.CharField(label=u'Ваше имя:', max_length=140)
	email = forms.EmailField(label=u'Почта для обратной связи:', max_length=140)
	photo = forms.ImageField(label=u'Приложите фотографию:', max_length=100, required=False)
	history = forms.CharField(label=u'Описание фотографии:', max_length=1000, widget=forms.Textarea)
