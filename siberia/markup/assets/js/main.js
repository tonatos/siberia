$(document).ready(function(){

	function declOfNum(number, titles) {  
	    cases = [2, 0, 1, 1, 1, 2];  
	    return titles[ (number%100>4 && number%100<20)? 2 : cases[(number%10<5)?number%10:5] ];  
	}

	function getCookie(name) {
	    var cookieValue = null;
	    if (document.cookie && document.cookie != '') {
	        var cookies = document.cookie.split(';');
	        for (var i = 0; i < cookies.length; i++) {
	            var cookie = jQuery.trim(cookies[i]);
	            // Does this cookie string begin with the name we want?
	            if (cookie.substring(0, name.length + 1) == (name + '=')) {
	                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
	                break;
	            }
	        }
	    }
	    return cookieValue;
	}
	var csrftoken = getCookie('csrftoken');

	function csrfSafeMethod(method) {
	    // these HTTP methods do not require CSRF protection
	    return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
	}
	$.ajaxSetup({
	    beforeSend: function(xhr, settings) {
	        if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
	            xhr.setRequestHeader("X-CSRFToken", csrftoken);
	        }
	    }
	});

	$('.carousel__inner').each(function(i, obj){
		var carousel = $(obj).owlCarousel({
			items: 1,
			itemsDesktop: false,
			itemsTablet: false,
			singleItem: true,
			pagination: true,
			paginationNumbers: false
		});
		$(obj).parent().find('.destiny-block__arrow').click(function(){
			carousel.trigger('owl.next');
			return false
		});
	});

	$('.js-selectregion').change(function(){
		location.href = location.pathname + '?region=' + $(this).prop('value')
	})
	$('.js-selectrubric').change(function(){
		location.href = location.pathname + '?rubric=' + $(this).prop('value')
	})

	$('.js-news-block__item-vote').click(function(){
		var $obj = $(this);

		$.post('/vote/', {pk: $(this).data('pk')}, function(data){
			console.log(data)
			if (data.status == true) {
				// $obj.before('<span class="news-block__item-vote-result">' +
				// 				data.votes + declOfNum(data.votes, [' голос', ' голоса', ' голосов']) +
				// 			'</span>');
				$obj.before('<span class="news-block__item-vote-result">Вы проголосовали</span>');

				$obj.remove();
			} else {
				alert('Произошла ошибка при отправке запроса');
			}
		})
	})

	$('.material-block__list-item--candidate .material-block__list-item-img').ready(function(){
		$('.material-block__list-item--candidate .material-block__list-item-img').height(
			$('.material-block__list-item--candidate').height()
		)
	})
})