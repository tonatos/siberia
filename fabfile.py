# coding: utf-8
import os
from fabric.api import *
from fabric.contrib import django

django.settings_module('siberia.settings')
from django.conf import settings


env.hosts = ['u52042.netangels.ru']
env.user = "u52042"

def update_django_project():
    """ Updates the remote django project.
    """
    local('git push')
    
    with cd('/home/u52042/sibhar.ru/siberia'):
        run('git pull')
        with prefix('source /home/u52042/.virtualenvs/siberia-env/bin/activate'):
            #run('pip install -r requirements.txt')
            run('python manage.py migrate') # if you use south
            run('python manage.py collectstatic --noinput')

def restart_webserver():
    """ Restarts remote nginx and uwsgi.
    """
    run('pkill -u u52042 -f django-wrapper.fcgi', shell_escape=True, combine_stderr=False)

def deploy():
    """ Deploy Django Project.
    """
    update_django_project()
    restart_webserver()

# https://auphonic.com/blog/2011/06/18/django-deployment-nginx-uwsgi-virtualenv-and-fabric/
# https://github.com/ronnix/fabtools
# https://github.com/ashokfernandez/Django-Fabric-AWS
# https://bitbucket.org/kmike/django-fab-deploy
# https://github.com/mocco/django-fabric
# https://github.com/hbussell/django-fab